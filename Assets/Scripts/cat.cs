﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class cat : MonoBehaviour {
	private GameObject MrRight;
	private NavMeshAgent navMesh;
	private float follow = 100f;
	private float stop = 5f;
	private AudioSource audiosource;
	public AudioClip audioclip;
	Animator anime;
	// Use this for initialization
	void Start () {
		MrRight = GameObject.FindGameObjectWithTag ("Player");
		navMesh = GetComponent<NavMeshAgent> ();
		anime = GetComponent<Animator> ();
		audiosource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance (transform.position, MrRight.transform.position) < follow) {
			anime.SetBool ("idle", false);
			anime.SetBool ("walk", true);
			navMesh.SetDestination (MrRight.transform.position);
			if (Vector3.Distance (transform.position, MrRight.transform.position) < stop) {
				anime.SetBool ("idle", true);
				anime.SetBool ("walk", false);
				meow ();
			}
		}
	}
	public void meow(){
		if (audiosource.isPlaying) {
		} else {
			audiosource.PlayOneShot (audioclip);
		}
	}

}
