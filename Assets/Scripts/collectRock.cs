﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class collectRock : MonoBehaviour {
	public int alienHit = 0;
	public Text aliens;
	public GameObject winText;
	public static collectRock Instance;
	void OnControllerColliderHit(ControllerColliderHit hit)
	{
		if (hit.gameObject.tag == "rock") {
			throwRock.Instance.rockCount++;
			Destroy (hit.gameObject);
			Debug.Log ("Savacu akmeni!");
		}

	}
	// Use this for initialization
	void Start () {
		Instance = this;
		//winText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		aliens.text = alienHit.ToString ()+ "/4";
		if (alienHit == 4) {
			StartCoroutine (win());
		} else {
			Debug.Log ("Kaut kas atkal nav");
		}
	}
	IEnumerator win(){
		winText.SetActive(true);
		yield return new WaitForSeconds (3f);
		winText.SetActive(false);
		Application.LoadLevel (0);

	}
}
