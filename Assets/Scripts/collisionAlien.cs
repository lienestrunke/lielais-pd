﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class collisionAlien : MonoBehaviour {
	private AudioSource audiosource;
	public AudioClip audioclip;
	void OnCollisionEnter(Collision coll)
	{
		if (coll.gameObject.tag == "alien_tag") {
			audiosource.PlayOneShot (audioclip);
			Destroy (coll.gameObject);
			collectRock.Instance.alienHit++;
			}
	}
	// Use this for initialization
	void Start () {
		audiosource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {

	}
}
