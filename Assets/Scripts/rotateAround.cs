﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateAround : MonoBehaviour {

	public float speed;
	public Transform target;

	private Vector3 yAxis = new Vector3(0, 1, 0);

	void FixedUpdate () {
		transform.RotateAround(target.position, yAxis, speed); 
	}
}
