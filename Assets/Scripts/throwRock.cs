﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class throwRock : MonoBehaviour {
	public GameObject begin;
	public GameObject rock;
	private GameObject rockHandler;
	public float force;
	//cik akmeni sakuma
	public int rockCount = 50;
	public Text rockNumber;
	public static throwRock Instance;
	// Use this for initialization
	void Start () {
		Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		rockNumber.text = rockCount.ToString ();
		//mouse0 = kreisais klik
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			if (rockCount > 0) {
				throwingRock();
				rockCount--;
			}
		}

	}
	private void throwingRock(){
		//izsauc un parveido par gameobject
		rockHandler = Instantiate(rock, begin.transform.position, begin.transform.rotation) as GameObject;
		rockHandler.transform.Rotate(Vector3.left *180);
		Rigidbody rockRigidbody = rockHandler.GetComponent<Rigidbody>();
		rockRigidbody.AddForce(transform.forward * force);
	}

}
